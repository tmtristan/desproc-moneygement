﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMode : MonoBehaviour {

    public GameObject Player;
    public float CashGoal;
    [HideInInspector]
    public bool WinGame;

    private Wallet walletScript;

    // Use this for initialization
    void Start () {

        walletScript = Player.GetComponent<Wallet>();
    }
	
	// Update is called once per frame
	void Update () {
		if(walletScript.CurrentMoney >= CashGoal)
        {
            WinGame = true;
        }
	}
}
