﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wallet : MonoBehaviour {

    public float CurrentMoney;
    public float Allowance;

    [HideInInspector]
    public float MoneyAtDayStart;
    [HideInInspector]
    public float MoneySpent;
    [HideInInspector]
    public float MoneyLeft;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void addMoney(float value)
    {
        CurrentMoney += value;
    }

    public void removeMoney(float value)
    {
        CurrentMoney -= value;
    }
}
