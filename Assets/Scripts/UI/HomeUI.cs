﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeUI : MonoBehaviour {

    public GameObject GameManager;
    public Button SchoolButton;
    public Button SleepButton;
    public Button EndDayButton;

    private TimePhases TimePhasesScript;

    // Use this for initialization
    void Start () {
        TimePhasesScript = GameManager.GetComponent<TimePhases>();
        EndDayButton.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		if(TimePhasesScript.currentTime == TimePhases.TimeStates.Evening && TimePhasesScript.ActionTaken == true)
        {
            SchoolButton.interactable = false;
            SleepButton.interactable = false;
            EndDayButton.gameObject.SetActive(true);
        }
        else
        {
            SchoolButton.interactable = true;
            SleepButton.interactable = true;
            EndDayButton.gameObject.SetActive(false);
        }
	}
}
