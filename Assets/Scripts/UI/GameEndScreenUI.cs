﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameEndScreenUI : MonoBehaviour {

    public GameObject GameManager;
    public GameObject VictoryScreenCanvas;

    private GameMode gameModeScript;

	// Use this for initialization
	void Start () {
        VictoryScreenCanvas.SetActive(false);
        gameModeScript = GameManager.GetComponent<GameMode>();
	}
	
	// Update is called once per frame
	void Update () {
		if(gameModeScript.WinGame == true)
        {
            VictoryScreenCanvas.SetActive(true);
        }
	}

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("mainmenu");
    }
}
