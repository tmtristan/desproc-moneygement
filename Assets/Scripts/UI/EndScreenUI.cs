﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenUI : MonoBehaviour {

    public GameObject Player;
    public GameObject GameManager;
    public GameObject EndScreenCanvas;
    public Text DayText;
    public Text MoneyAtStartText;
    public Text MoneySpentText;
    public Text MoneyLeftText;

    private Wallet walletScript;
    private TimePhases timePhasesScript;
    private bool showUI;

    // Use this for initialization
    void Start () {
        walletScript = Player.GetComponent<Wallet>();
        timePhasesScript = GameManager.GetComponent<TimePhases>();
        showUI = false;
        EndScreenCanvas.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
    }

    public void ShowScreen()
    {
        if (showUI == false)
        {
            showUI = true;
            UpdateText();
            EndScreenCanvas.SetActive(true);
        }
        else
        {
            showUI = false;
            EndScreenCanvas.SetActive(false);
        }
    }

    public void UpdateText()
    {
        DayText.text = "End of Day " + timePhasesScript.Day.ToString();
        MoneyAtStartText.text = "Php " + walletScript.MoneyAtDayStart.ToString("F2");
        MoneySpentText.text = "Php " + walletScript.MoneySpent.ToString("F2");
        MoneyLeftText.text = "Php " + walletScript.CurrentMoney.ToString("F2");
    }
}
