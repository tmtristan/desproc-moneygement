﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StressUI : MonoBehaviour {

    public GameObject Player;
    public Text CurrentStressText;
    public Text DividerText;
    public Text MaxStressText;

    private Stress stressScript;

    // Use this for initialization
    void Start () {
        stressScript = Player.GetComponent<Stress>();
	}
	
	// Update is called once per frame
	void Update () {
        CurrentStressText.text = stressScript.CurrentStress.ToString();
        MaxStressText.text = stressScript.MaxStress.ToString();
        if(stressScript.CurrentStress >= stressScript.MaxStress)
        {
            CurrentStressText.color = Color.red;
            DividerText.color = Color.red;
            MaxStressText.color = Color.red;
        }
        else
        {
            CurrentStressText.color = Color.black;
            DividerText.color = Color.black;
            MaxStressText.color = Color.black;
        }
	}
}
