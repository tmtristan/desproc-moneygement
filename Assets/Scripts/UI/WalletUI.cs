﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WalletUI : MonoBehaviour {

    public GameObject Player;
    public GameObject GameManager;
    public GameObject WalletDisplay;
    public Text MoneyText;
    public Text GoalText;

    private Wallet walletScript;
    private GameMode gameModeScript;
    private bool showUI;

    private void Start()
    {
        showUI = false;
        WalletDisplay.SetActive(false);
        walletScript = Player.GetComponent<Wallet>();
        gameModeScript = GameManager.GetComponent<GameMode>();

        GoalText.text = "Save PHP " + gameModeScript.CashGoal.ToString("F2") + ".";
    }

    private void Update()
    {
        MoneyText.text = "Php " + walletScript.CurrentMoney.ToString("F2"); //F2 rounds it to two decimals.
        if (walletScript.CurrentMoney <= 0)
        {
            MoneyText.color = Color.red;
        }
        else
        {
            MoneyText.color = Color.black;
        }
    }

    public void ShowButton()
    {
        if(showUI == false)
        {
            showUI = true;
            WalletDisplay.SetActive(true);
        }
        else
        {
            showUI = false;
            WalletDisplay.SetActive(false);
        }
    }
}
