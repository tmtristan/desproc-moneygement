﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimePhasesUI : MonoBehaviour {

    public GameObject GameManager;
    public Text DayText;
    public Text TimeText;

    private TimePhases TimePhasesScript;

	// Use this for initialization
	void Start () {
        TimePhasesScript = GameManager.GetComponent<TimePhases>();
	}
	
	// Update is called once per frame
	void Update () {
        DayText.text = TimePhasesScript.Day.ToString();
        TimeText.text = TimePhasesScript.currentTime.ToString();
	}
}
