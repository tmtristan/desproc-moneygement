﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stress : MonoBehaviour {

    public int CurrentStress;
    public int MaxStress;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void addStress(int value)
    {
        CurrentStress += value;
    }

    public void removeStress(int value)
    {
        CurrentStress -= value;
        if(CurrentStress < 0)
        {
            CurrentStress = 0;
        }
    }
}
