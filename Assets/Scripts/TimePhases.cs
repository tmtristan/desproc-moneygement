﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimePhases : MonoBehaviour {

    [HideInInspector]
    public enum TimeStates { Morning, Afternoon, Evening};
    [HideInInspector]
    public TimeStates currentTime;
    public GameObject Player;
    [HideInInspector]
    public bool ActionTaken;

    public int Day;

    private Wallet walletScript;

    // Use this for initialization
    void Start () {
        walletScript = Player.GetComponent<Wallet>();
        DayStart();
        currentTime = TimeStates.Morning;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeTime()
    {
        if (currentTime == TimeStates.Morning && ActionTaken == true)
        {
            RestoreAction();
            currentTime = TimeStates.Afternoon;
        }
        else if (currentTime == TimeStates.Afternoon && ActionTaken == true)
        {
            RestoreAction();
            currentTime = TimeStates.Evening;
        }
        else if (currentTime == TimeStates.Evening && ActionTaken == true)
        {
            Day += 1;
            DayStart();
            currentTime = TimeStates.Morning;
        }
    }

    public void RestoreAction()
    {
        ActionTaken = false;
    }

    public void DoAction()
    {
        ActionTaken = true;
        if(currentTime != TimeStates.Evening)
        {
            ChangeTime();
        }
    }

    private void DayStart()
    {
        RestoreAction();
        walletScript.addMoney(walletScript.Allowance);
        walletScript.MoneyAtDayStart = walletScript.CurrentMoney;
    }
}
